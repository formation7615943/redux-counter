import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
const counterSlice = createSlice({
  name: "counter",
  initialState: {
    count: 0,
    message: "",
    loading: false,
    error: false,
  },
  reducers: {
    increase: (state, action) => {
      state.count++;
    },
    decrease: (state, action) => {
      state.count--;
    },
    reset: (state, action) => {
      state.count = 0;
    },
    add: (state, action) => {
      state.count += action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(inscreaseAsync.pending, (state) => {
        state.loading = true;
      })
      .addCase(inscreaseAsync.fulfilled, (state, action) => {
        state.loading = false;
        state.count++;
        state.message = action.payload.message;
      })
      .addCase(inscreaseAsync.rejected, (state) => {
        state.loading = false;
        state.error = true;
      });
  },
});

export const inscreaseAsync = createAsyncThunk(
  "counter/inscreaseAsync",
  async (/* Object containing the data sent from form component */) => {
    const timeMs = 700;
    await delay(timeMs);
    return { message: `Received after ${timeMs}` };
  }
);

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export const { increase, decrease, reset, add } = counterSlice.actions;
export default counterSlice.reducer;
