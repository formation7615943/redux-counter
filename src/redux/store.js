import { configureStore } from "@reduxjs/toolkit";
import { rootReducer } from "./slices";

const store = configureStore({
  reducer: rootReducer,
});

console.log("store", store.getState());
export default store;
