import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  decrease,
  increase,
  reset,
  add,
  inscreaseAsync,
} from "../redux/slices/counter.slices";

export const CounterNav = ({ count, setCount }) => {
  const [number, setNmber] = useState(0);
  const dispatch = useDispatch();
  return (
    <div className="counter-nav">
      <button
        onClick={() => {
          // setCount(count - 1)
          dispatch(decrease());
        }}
      >
        Decrease
      </button>

      <button
        onClick={() => {
          // setCount(0)
          dispatch(reset());
        }}
      >
        Reset
      </button>

      <button
        onClick={() => {
          // setCount(count + 1)
          dispatch(inscreaseAsync());
        }}
      >
        Increase
      </button>

      <input
        type="number"
        value={number}
        onChange={(e) => setNmber(+e.target.value)}
      />
      <button onClick={() => dispatch(add(number))}>Add</button>
    </div>
  );
};

export default CounterNav;
